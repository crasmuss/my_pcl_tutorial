
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// sample PCL in ROS Hydro
// Christopher Rasmussen
// 2014
// based on PCL tutorials and Hydro migration docs
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#include <ros/ros.h>

#include <pcl_conversions/pcl_conversions.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/filters/passthrough.h>

#include <pcl/filters/extract_indices.h>

//----------------------------------------------------------------------------

ros::Publisher in_pub;
ros::Publisher out_pub;

//----------------------------------------------------------------------------

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud

  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::fromROSMsg (*input, cloud);

  printf("got %i points\n", cloud.points.size()); fflush(stdout);

  pcl::PointCloud<pcl::PointXYZ> cloud_inliers;
  pcl::PointCloud<pcl::PointXYZ> cloud_outliers;

  // Create the filtering object

  pcl::PassThrough<pcl::PointXYZ> pass;
  pass.setInputCloud (cloud.makeShared());

  /*  
  pass.setFilterFieldName ("x");
  pass.setFilterLimits (0.0, 2.0);
  */

  pass.setFilterFieldName ("y");
  pass.setFilterLimits (-2.0, 2.0);

  // perform filtering

  pass.filter(cloud_inliers);

  pass.setFilterLimitsNegative (true);
  pass.filter(cloud_outliers);

  printf("%i inliers, %i outliers\n", cloud_inliers.points.size(), cloud_outliers.points.size());

  // publish results

  sensor_msgs::PointCloud2 inliers_cloud_msg;

  pcl::toROSMsg(cloud_inliers, inliers_cloud_msg);
  inliers_cloud_msg.header.frame_id = "base_link"; 
  in_pub.publish(inliers_cloud_msg);

  sensor_msgs::PointCloud2 outliers_cloud_msg;

  pcl::toROSMsg(cloud_outliers, outliers_cloud_msg);
  outliers_cloud_msg.header.frame_id = "base_link"; 
  out_pub.publish(outliers_cloud_msg);
 
}

//----------------------------------------------------------------------------

int main (int argc, char** argv)
{
  printf("starting up passthrough\n"); fflush(stdout);

  // Initialize ROS

  ros::init (argc, argv, "passthrough");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud

  ros::Subscriber sub = nh.subscribe ("cloud_pcd", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud

  in_pub = nh.advertise<sensor_msgs::PointCloud2> ("inliers", 1);
  out_pub = nh.advertise<sensor_msgs::PointCloud2> ("outliers", 1);

  // Spin

  ros::spin ();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
