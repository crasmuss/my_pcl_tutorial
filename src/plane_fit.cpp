
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// sample PCL in ROS Hydro
// Christopher Rasmussen
// 2014
// based on PCL tutorials and Hydro migration docs
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#include <ros/ros.h>

#include <pcl_conversions/pcl_conversions.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/filters/extract_indices.h>

//----------------------------------------------------------------------------

ros::Publisher in_pub;
ros::Publisher out_pub;

//----------------------------------------------------------------------------

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud

  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::fromROSMsg (*input, cloud);

  printf("got %i points\n", cloud.points.size()); fflush(stdout);

  // Create the segmentation object

  pcl::ModelCoefficients coefficients;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

  pcl::SACSegmentation<pcl::PointXYZ> seg;

  // Set parameters

  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (0.1);
  seg.setInputCloud (cloud.makeShared ());

  // perform the RANSAC fit

  seg.segment (*inliers, coefficients);

  // did it work?

  if (inliers->indices.size () == 0) {
    printf("Could not estimate a planar model for the given dataset\n");
    return;
  }

  std::cerr << "Model coefficients: " << coefficients.values[0] << " " 
                                      << coefficients.values[1] << " "
                                      << coefficients.values[2] << " " 
                                      << coefficients.values[3] << std::endl;

  // we actually only got indices of inliers, so we need an additional step to get the actual points

  pcl::PointCloud<pcl::PointXYZ> cloud_inliers;
  pcl::PointCloud<pcl::PointXYZ> cloud_outliers;

  pcl::ExtractIndices<pcl::PointXYZ> extract;
  
  // extract inliers
  
  extract.setInputCloud(cloud.makeShared());
  extract.setIndices(inliers);
  extract.setNegative(false);
  extract.filter(cloud_inliers);
    
  extract.setNegative (true);
  extract.filter (cloud_outliers);

  // publish results

  printf("%i inliers, %i outliers\n", cloud_inliers.points.size(), cloud_outliers.points.size());

  sensor_msgs::PointCloud2 inliers_cloud_msg;

  pcl::toROSMsg(cloud_inliers, inliers_cloud_msg);
  inliers_cloud_msg.header.frame_id = "base_link"; 
  in_pub.publish(inliers_cloud_msg);

  sensor_msgs::PointCloud2 outliers_cloud_msg;

  pcl::toROSMsg(cloud_outliers, outliers_cloud_msg);
  outliers_cloud_msg.header.frame_id = "base_link"; 
  out_pub.publish(outliers_cloud_msg);
 
}

//----------------------------------------------------------------------------

int main (int argc, char** argv)
{
  printf("starting up plane_fit\n"); fflush(stdout);

  // Initialize ROS

  ros::init (argc, argv, "plane_fit");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud

  ros::Subscriber sub = nh.subscribe ("cloud_pcd", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud

  in_pub = nh.advertise<sensor_msgs::PointCloud2> ("inliers", 1);
  out_pub = nh.advertise<sensor_msgs::PointCloud2> ("outliers", 1);

  // Spin

  ros::spin ();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
