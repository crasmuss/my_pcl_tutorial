#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/filters/filter.h>

#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>

#include <iostream>

int main (int argc, char **argv)
{
  ros::init (argc, argv, "tf");
  ros::NodeHandle nh;

static tf::TransformBroadcaster tf_br;

  Eigen::Matrix4f M_transf;   // this is what ICP returns
  Eigen::Matrix4d M_transd;   // intermediate form for type conversion
  tf::Transform M_tf;         // this is what we send to Rviz -- in Displays just Add a TF

  // initialize to identity

  M_transf << 
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1;

  ros::Rate loop_rate(10);

  while (ros::ok()) {

    // this line will make the axes slowly translate in X direction

    M_transf(0, 3) += 0.01;

    M_transd = M_transf.cast<double>();
    Eigen::Affine3d M_affine(M_transd);
    tf::transformEigenToTF(M_affine, M_tf);
    
    tf_br.sendTransform(tf::StampedTransform(M_tf, ros::Time::now(), "map", "sensor"));

    ros::spinOnce ();

    loop_rate.sleep();

  }

  return 0;
}
