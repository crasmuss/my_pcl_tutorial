
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// sample PCL in ROS Hydro
// Christopher Rasmussen
// 2014
// based on PCL tutorial http://pointclouds.org/documentation/tutorials/normal_estimation.php
// and Hydro migration docs
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#include <ros/ros.h>

#include <pcl_conversions/pcl_conversions.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/filters/extract_indices.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/features/normal_3d.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>

//----------------------------------------------------------------------------

using namespace std;

#ifndef DEG2RAD
#define DEG2RAD(x)            ((x) * 0.017453)
#endif

#ifndef RAD2DEG
#define RAD2DEG(x)            ((x) * 57.29578)
#endif

//----------------------------------------------------------------------------

// function declarations

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr&);

//----------------------------------------------------------------------------

bool units_are_mm = false;

ros::NodeHandle *nhp = NULL;

// subscribers

ros::Subscriber cloud_sub;

// publishers

ros::Publisher filtered_input_pub;
ros::Publisher normals_color_pub;
ros::Publisher cylinder_marker_array_pub;

// point clouds

pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloudptr;
pcl::PointCloud<pcl::PointXYZ>::Ptr close_input_cloudptr;
pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_input_cloudptr;
pcl::PointCloud<pcl::Normal>::Ptr normals_cloudptr;
pcl::PointCloud<pcl::PointXYZRGB>::Ptr normals_color_cloudptr;

//----------------------------------------------------------------------------

void initialize_ros(int argc, char **argv, string & s)
{
  // Initialize ROS

  ros::init (argc, argv, s);

  nhp = new ros::NodeHandle();

  // ROS subscribers

  cloud_sub = nhp->subscribe ("cloud_pcd", 1, cloud_cb);

  // ROS publishers

  filtered_input_pub = nhp->advertise<sensor_msgs::PointCloud2> ("filtered_input", 1);
  normals_color_pub = nhp->advertise<sensor_msgs::PointCloud2> ("normals", 1);
  cylinder_marker_array_pub = nhp->advertise<visualization_msgs::MarkerArray>("cylinder_marker_array", 10);

  // allocate point clouds

  input_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZ>);
  close_input_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZ>);
  filtered_input_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZ>);
  normals_cloudptr.reset(new pcl::PointCloud<pcl::Normal>);
  normals_color_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZRGB>);

}

//----------------------------------------------------------------------------

void scale_in_place(double scale_factor, pcl::PointCloud<pcl::PointXYZ>::Ptr cloudptr)
{
  for (int i = 0; i < cloudptr->points.size(); i++) {
    cloudptr->points[i].x *= scale_factor;
    cloudptr->points[i].y *= scale_factor;
    cloudptr->points[i].z *= scale_factor;
  }
}

//----------------------------------------------------------------------------

// only "pass" points closer (radially) than max_distance

void distance_filter(double max_distance, pcl::PointCloud<pcl::PointXYZ>::Ptr cloudptr, pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloudptr)
{
  pcl::PointXYZ P;
  double squared_distance;
  double max_squared_distance = max_distance * max_distance;

  output_cloudptr->points.clear();

  for (int i = 0; i < cloudptr->points.size(); i++) {

    P = cloudptr->points[i];

    squared_distance = P.x * P.x + P.y * P.y + P.z * P.z;

    if (squared_distance <= max_squared_distance)
      output_cloudptr->points.push_back(P);
  }
}

//----------------------------------------------------------------------------

void voxelize(float leaf_size, pcl::PointCloud<pcl::PointXYZ>::Ptr cloudptr, pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloudptr)
{
  pcl::VoxelGrid<pcl::PointXYZ> sor;
  sor.setInputCloud (cloudptr);
  sor.setLeafSize (leaf_size, leaf_size, leaf_size);

  // Do the filtering

  sor.filter (*output_cloudptr);

  printf("%i voxelized points [from %i]\n", output_cloudptr->points.size(), cloudptr->points.size());
}

//----------------------------------------------------------------------------

// returns false if there was a problem

void estimate_normals(float neighbor_radius,
		      pcl::PointCloud<pcl::PointXYZ>::Ptr cloudptr, 
		      pcl::PointCloud<pcl::Normal>::Ptr norm_cloudptr)
{
    // Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
  ne.setInputCloud (cloudptr);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  // Use all neighbors in a sphere of radius 3cm
  ne.setRadiusSearch (neighbor_radius);

  // Compute the features
  ne.compute(*norm_cloudptr);
}

//----------------------------------------------------------------------------

// send a bunch of cylinder markers at once for Rviz display

void send_cylinder_marker_array(vector <pcl::PointXYZ> & P,     // center positions
				vector <double> & roll, vector <double> & pitch, vector <double> & yaw,
				vector <double> & radius,
				vector <double> & length,
				vector <float> & r, vector <float> g, vector <float> & b, 
				float alpha)
{
  // Initialize common marker parameters

  visualization_msgs::MarkerArray cyl_array;

  cyl_array.markers.clear();

  visualization_msgs::Marker cyl;
  cyl.header.frame_id = "/base_link";
  cyl.header.stamp = ros::Time::now();
  cyl.ns = "marker";
  cyl.action = visualization_msgs::Marker::ADD;
  cyl.type = visualization_msgs::Marker::CYLINDER;
  cyl.color.a = alpha;
    
  // iterate over cylinders

  for (int i = 0; i < P.size(); i++) {

    cyl.pose.position.x = P[i].x;
    cyl.pose.position.y = P[i].y;
    cyl.pose.position.z = P[i].z;

    tf::Quaternion q = tf::createQuaternionFromRPY(roll[i], pitch[i], yaw[i]);

    cyl.pose.orientation.x = q.x();
    cyl.pose.orientation.y = q.y();
    cyl.pose.orientation.z = q.z();
    cyl.pose.orientation.w = q.w();
  
    // ID

    cyl.id = 33 + i;
  
    // Scale

    cyl.scale.x = 2.0*radius[i];
    cyl.scale.y = 2.0*radius[i];
    cyl.scale.z = length[i];
  
    // Color

    cyl.color.r = r[i];
    cyl.color.g = g[i];
    cyl.color.b = b[i];

    cyl_array.markers.push_back(cyl);
  }

  cylinder_marker_array_pub.publish(cyl_array);
}

//----------------------------------------------------------------------------

// just to show syntax

void send_sample_cylinders()
{
  pcl::PointXYZ P;
  vector <pcl::PointXYZ> P_cyl;
  vector <double> roll_cyl;
  vector <double> pitch_cyl;
  vector <double> yaw_cyl;
  vector <double> radius_cyl;
  vector <double> length_cyl;
  vector <float> r_cyl;
  vector <float> g_cyl;
  vector <float> b_cyl;

  // #1 

  P.x = 3.0;
  P.y = 0.0;
  P.z = 0.0;
  P_cyl.push_back(P);

  roll_cyl.push_back(0.0);
  pitch_cyl.push_back(0.0);
  yaw_cyl.push_back(0.0);

  radius_cyl.push_back(0.2);
  length_cyl.push_back(3.0);  
  
  r_cyl.push_back(255.0);
  g_cyl.push_back(0.0);
  b_cyl.push_back(0.0);

  // #2 

  P.x = 0.0;
  P.y = 0.0;
  P.z = 0.0;
  P_cyl.push_back(P);

  roll_cyl.push_back(0.0);
  pitch_cyl.push_back(DEG2RAD(90.0));
  yaw_cyl.push_back(DEG2RAD(45.0));

  radius_cyl.push_back(0.5);
  length_cyl.push_back(2.0);  
  
  r_cyl.push_back(0.0);
  g_cyl.push_back(255.0);
  b_cyl.push_back(0.0);

  // #3 

  P.x = 0.0;
  P.y = -3.0;
  P.z = 0.0;
  P_cyl.push_back(P);

  roll_cyl.push_back(0.0);
  pitch_cyl.push_back(DEG2RAD(45.0));
  yaw_cyl.push_back(DEG2RAD(-45.0));

  radius_cyl.push_back(0.1);
  length_cyl.push_back(4.0);  
  
  r_cyl.push_back(0.0);
  g_cyl.push_back(0.0);
  b_cyl.push_back(255.0);

  // publish!

  send_cylinder_marker_array(P_cyl,
			     roll_cyl, pitch_cyl, yaw_cyl,
			     radius_cyl, length_cyl,
			     r_cyl, g_cyl, b_cyl, 
			     0.75);

}

//----------------------------------------------------------------------------

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud

  pcl::fromROSMsg (*input, *input_cloudptr);

  // in case the units are different from what we expect

  if (units_are_mm)
    scale_in_place(0.001, input_cloudptr);

  // distance filter to get rid of distant stuff 

  distance_filter(10.0, input_cloudptr, close_input_cloudptr);

  // voxelize to put a cap on size

  voxelize(0.025, close_input_cloudptr, filtered_input_cloudptr);

  // publish this

  sensor_msgs::PointCloud2 filtered_input_cloud_msg;

  pcl::toROSMsg(*filtered_input_cloudptr, filtered_input_cloud_msg);
  filtered_input_cloud_msg.header.frame_id = "base_link"; 
  filtered_input_pub.publish(filtered_input_cloud_msg);

  // estimate normals

  estimate_normals(0.05, filtered_input_cloudptr, normals_cloudptr);

  // "colorize" cloud by normals and publish

  pcl::PointXYZRGB P_color;

  normals_color_cloudptr->points.clear();

  for (int i = 0; i < filtered_input_cloudptr->points.size(); i++) {
    P_color.x = filtered_input_cloudptr->points[i].x;
    P_color.y = filtered_input_cloudptr->points[i].y;
    P_color.z = filtered_input_cloudptr->points[i].z;
    P_color.r = 255.0 * fabs(normals_cloudptr->points[i].normal_x);
    P_color.g = 255.0 * fabs(normals_cloudptr->points[i].normal_y);
    P_color.b = 255.0 * fabs(normals_cloudptr->points[i].normal_z);

    normals_color_cloudptr->points.push_back(P_color);

  }

  sensor_msgs::PointCloud2 normals_color_cloud_msg;

  pcl::toROSMsg(*normals_color_cloudptr, normals_color_cloud_msg);
  normals_color_cloud_msg.header.frame_id = "base_link"; 
  normals_color_pub.publish(normals_color_cloud_msg);

  // sample cylinder markers

  send_sample_cylinders();
}

//----------------------------------------------------------------------------

int main (int argc, char** argv)
{
  // handle command-line flags

  for (int i = 0; i < argc; i++) {
    if (!strcmp(argv[i], "-mm"))
      units_are_mm = true;
  }

  // initialization

  string s("normals");

  printf("starting up %s\n", s.c_str()); fflush(stdout);

  initialize_ros(argc, argv, s);

  // Spin

  ros::spin ();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
