
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// sample PCL in ROS Hydro
// Christopher Rasmussen
// 2014
// based on PCL tutorial http://pointclouds.org/documentation/tutorials/cluster_extraction.php#cluster-extraction
// and Hydro migration docs
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#include <ros/ros.h>

#include <pcl_conversions/pcl_conversions.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/filters/extract_indices.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

//----------------------------------------------------------------------------

using namespace std;

#ifndef DEG2RAD
#define DEG2RAD(x)            ((x) * 0.017453)
#endif

#ifndef RAD2DEG
#define RAD2DEG(x)            ((x) * 57.29578)
#endif

//----------------------------------------------------------------------------

// function declarations

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr&);

//----------------------------------------------------------------------------

ros::NodeHandle *nhp = NULL;

// subscribers

ros::Subscriber cloud_sub;

// publishers

ros::Publisher in_pub;
ros::Publisher out_pub;

int max_clusters = 5;
vector <ros::Publisher> cluster_pub;
vector <sensor_msgs::PointCloud2> cluster_cloud_msg;

// point clouds

pcl::PointCloud<pcl::PointXYZ>::Ptr input_cloudptr;
pcl::PointCloud<pcl::PointXYZ>::Ptr close_input_cloudptr;
pcl::PointCloud<pcl::PointXYZ>::Ptr inliers_cloudptr;
pcl::PointCloud<pcl::PointXYZ>::Ptr outliers_cloudptr;

//----------------------------------------------------------------------------

void initialize_ros(int argc, char **argv, string & s)
{
  // Initialize ROS

  ros::init (argc, argv, s);

  nhp = new ros::NodeHandle();

  // ROS subscribers

  cloud_sub = nhp->subscribe ("cloud_pcd", 1, cloud_cb);

  // ROS publishers

  in_pub = nhp->advertise<sensor_msgs::PointCloud2> ("inliers", 1);
  out_pub = nhp->advertise<sensor_msgs::PointCloud2> ("outliers", 1);

  char *str = (char *) malloc(64 * sizeof(char));

  cluster_cloud_msg.resize(max_clusters);
  cluster_pub.resize(max_clusters);

  for (int i = 0; i < max_clusters; i++) {
    sprintf(str, "cluster%i", i);
    cluster_pub[i] = nhp->advertise<sensor_msgs::PointCloud2> (string(str), 1);
  }

  // allocate point clouds

  input_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZ>);
  close_input_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZ>);
  inliers_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZ>);
  outliers_cloudptr.reset(new pcl::PointCloud<pcl::PointXYZ>);

}

//----------------------------------------------------------------------------

// only "pass" points closer (radially) than max_distance

void distance_filter(double max_distance, pcl::PointCloud<pcl::PointXYZ>::Ptr cloudptr, pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloudptr)
{
  pcl::PointXYZ P;
  double squared_distance;
  double max_squared_distance = max_distance * max_distance;

  output_cloudptr->points.clear();

  for (int i = 0; i < cloudptr->points.size(); i++) {

    P = cloudptr->points[i];

    squared_distance = P.x * P.x + P.y * P.y + P.z * P.z;

    if (squared_distance <= max_squared_distance)
      output_cloudptr->points.push_back(P);
  }
}

//----------------------------------------------------------------------------

// returns false if there was a problem

bool plane_fit(float distance_threshold, 
	       pcl::PointCloud<pcl::PointXYZ>::Ptr cloudptr, 
	       pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloudptr, pcl::PointCloud<pcl::PointXYZ>::Ptr out_cloudptr,
	       double & plane_a, double & plane_b, double & plane_c, double & plane_d)
{
  // Create the segmentation object
  
  pcl::ModelCoefficients coefficients;
  pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

  pcl::SACSegmentation<pcl::PointXYZ> seg;


  // Set parameters

  seg.setOptimizeCoefficients (true);
  seg.setModelType (pcl::SACMODEL_PLANE);
  seg.setMethodType (pcl::SAC_RANSAC);
  seg.setDistanceThreshold (distance_threshold);
  seg.setInputCloud (cloudptr);

  // perform the RANSAC fit

  seg.segment (*inliers, coefficients);

  // did it work?

  if (inliers->indices.size () == 0) {
    printf("Could not estimate a planar model for the given dataset\n");
    return false;
  }

  // these can be used to compute point-to-plane distance later if we want
  // (see http://docs.pointclouds.org/1.7.1/group__sample__consensus.html)

  plane_a = coefficients.values[0];
  plane_b = coefficients.values[1];
  plane_c = coefficients.values[2];
  plane_d = coefficients.values[3];

  printf("plane parameters: %.3lf %.3lf %.3lf %.3lf\n", plane_a, plane_b, plane_c, plane_d);

  // we actually only got indices of inliers, so we need an additional step to get the actual points

  pcl::ExtractIndices<pcl::PointXYZ> extract;
  
  // extract inliers
  
  extract.setInputCloud(cloudptr);
  extract.setIndices(inliers);
  extract.setNegative(false);
  extract.filter(*inliers_cloudptr);
    
  extract.setNegative (true);
  extract.filter (*outliers_cloudptr);


  // success!

  return true;
}

//----------------------------------------------------------------------------

bool euclidean_cluster(float tolerance, float min_size, float max_size,
		       pcl::PointCloud<pcl::PointXYZ>::Ptr cloudptr,
		       vector <pcl::PointCloud<pcl::PointXYZ>::Ptr> & cluster_cloudptr)
{
  // Creating the KdTree object for the search method of the extraction

  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (cloudptr);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  ec.setClusterTolerance (tolerance); 
  ec.setMinClusterSize (min_size);
  ec.setMaxClusterSize (max_size);
  ec.setSearchMethod (tree);
  ec.setInputCloud (cloudptr);
  ec.extract (cluster_indices);

  cluster_cloudptr.clear();
  int j = 0;
  for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
    for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); pit++)
      cloud_cluster->points.push_back (outliers_cloudptr->points[*pit]); //*
    cloud_cluster->width = cloud_cluster->points.size ();
    cloud_cluster->height = 1;
    cloud_cluster->is_dense = false; //true;
    std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;

    cluster_cloudptr.push_back(cloud_cluster);

  }

  return true;
}

//----------------------------------------------------------------------------

void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
  // Convert the sensor_msgs/PointCloud2 data to pcl/PointCloud

  pcl::fromROSMsg (*input, *input_cloudptr);

  // distance filter to get rid of distant stuff

  distance_filter(4.0, input_cloudptr, close_input_cloudptr);

  // try to do a plane fit

  double plane_a, plane_b, plane_c, plane_d;

  if (!plane_fit(0.05, close_input_cloudptr, inliers_cloudptr, outliers_cloudptr, plane_a, plane_b, plane_c, plane_d))
    return;

  // publish plane inliers/outliers

  sensor_msgs::PointCloud2 inliers_cloud_msg;

  pcl::toROSMsg(*inliers_cloudptr, inliers_cloud_msg);
  inliers_cloud_msg.header.frame_id = "base_link"; 
  in_pub.publish(inliers_cloud_msg);

  sensor_msgs::PointCloud2 outliers_cloud_msg;

  pcl::toROSMsg(*outliers_cloudptr, outliers_cloud_msg);
  outliers_cloud_msg.header.frame_id = "base_link"; 
  out_pub.publish(outliers_cloud_msg);

  // cluster and publish

  vector <pcl::PointCloud<pcl::PointXYZ>::Ptr> cluster_cloudptr;

  euclidean_cluster(0.1, 1000, 250000, outliers_cloudptr, cluster_cloudptr);

  for (int j = 0; j < min((int) cluster_cloudptr.size(), max_clusters); j++) {

      pcl::toROSMsg(*cluster_cloudptr[j], cluster_cloud_msg[j]);
      cluster_cloud_msg[j].header.frame_id = "base_link"; 
      cluster_pub[j].publish(cluster_cloud_msg[j]);

  }
}

//----------------------------------------------------------------------------

int main (int argc, char** argv)
{
  // initialization

  string s("cluster_extract");

  printf("starting up %s\n", s.c_str()); fflush(stdout);

  initialize_ros(argc, argv, s);

  // Spin

  ros::spin ();
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
